from setuptools import setup, find_packages
import sys, os

version = '0.1'

setup(name='cfz-persistence',
      version=version,
      description="Persistence layer for cfzcore.",
      long_description="""\
""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='persistence, cfzcore',
      author='Roberto Guerra',
      author_email='uris77@gmail.com',
      url='',
      license='Apache',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      test_suite='nose.collector',
      install_requires=[
	#'sqlalchemy == 0.7.9', 'psycopg2'
	'sqlalchemy', 'psycopg2'
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
