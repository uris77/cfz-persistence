# -*- coding: utf-8 -*-
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.schema import MetaData

sqlalchemy_url = 'postgres://cfzadmin:changeme@localhost:5432/cfz-test'
engine = create_engine(sqlalchemy_url)
maker = sessionmaker(autocommit=True)
DBSession = scoped_session(maker)
metadata = MetaData(sqlalchemy_url)


def init_session():
    DBSession.configure(bind=engine)


def get_session():
    return DBSession
