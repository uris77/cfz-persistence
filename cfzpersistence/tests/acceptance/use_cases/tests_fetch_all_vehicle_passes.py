from nose.tools import eq_

from cfzpersistence.tests import (DaoTest,
                                  DBSession)
from cfzpersistence.repository.vehicle_pass import VehiclePassRepository

from cfzcore.person import Person
from cfzcore.company import Company
from cfzcore.interactors.vehicle_pass import fetch_all_vehicle_passes

from collections import namedtuple


class FetchAllVehiclePassesTests(DaoTest):

    def test_fetch_all_vehicle_passes(self):
        vehicle_pass_repository = VehiclePassRepository(DBSession)
        company1 = self.create_company('Company1')
        for cnt in range(0, 20):
            self.create_vehicle_pass(company1)
        company2 = self.create_company('Company2')
        for cnt in range(0, 110):
            self.create_vehicle_pass(company2)
        all_passes = fetch_all_vehicle_passes(vehicle_pass_repository)
        eq_(130, len(all_passes))
