from nose.tools import eq_

from cfzpersistence.tests import (DaoTest, DBSession)
from cfzpersistence.repository.vehicle_pass import VehiclePassRepository

from cfzcore.interactors.vehicle_pass import search_for_vehicle_pass_by_owner


class SearchForAllVehiclePassesForAnOwner(DaoTest):
    
    def test_search_for_all_vehicle_passes_for_an_owner(self):
        vehicle_pass_repository = VehiclePassRepository(DBSession)
        company = self.create_company(u"Company Name")
        owner_name = u"Owner OfVehicle"
        for cnt in range(0,10):
            self.create_vehicle_pass(company, owner_name)
        vehicle_passes_for_owner = search_for_vehicle_pass_by_owner(owner_name,
                                                                    vehicle_pass_repository)
        eq_(10, len(vehicle_passes_for_owner))
