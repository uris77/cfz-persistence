from nose.tools import eq_

from cfzpersistence.tests import (DaoTest,
                                  DBSession)
from cfzpersistence.repository.vehicle_pass import VehiclePassRepository

from cfzcore.interactors.vehicle_pass import fetch_all_vehicle_passes_for_company



class FetchVehiclePassesForCompanyTests(DaoTest):


    def test_fetch_all_passes_for_company1_should_return_10(self):
        self._initialize_passes()
        passes_for_company1 = fetch_all_vehicle_passes_for_company(self.vehicle_pass_repository,
                                                                   self.company1.id)
        eq_(10, len(passes_for_company1))

    def test_fetch_all_passes_for_company2_should_return_100(self):
        self._initialize_passes()
        passes_for_company2 = fetch_all_vehicle_passes_for_company(self.vehicle_pass_repository,
                                                                   self.company2.id)
        eq_(100, len(passes_for_company2))

    def _initialize_passes(self):
        self.vehicle_pass_repository = VehiclePassRepository(DBSession)
        self.company1 = self.create_company("Company1")
        self.company2 = self.create_company("Company2")
        for cnt in range(0, 10):
            self.create_vehicle_pass(self.company1)
        for cnt in range(0, 100):
            self.create_vehicle_pass(self.company2)
