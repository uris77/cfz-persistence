from cfzpersistence.tests import (DaoTest,
                                  create_database,
                                  DBSession)
from cfzpersistence.repository.company import CompanyRepository
from cfzpersistence.repository.vehicle_pass import VehiclePassRepository

from cfzcore.person import Person
from cfzcore.company import Company
from cfzcore.interactors.vehicle_pass import create_vehicle_pass

from collections import namedtuple


class CreateVehiclePassTests(DaoTest):

    def test_should_create_a_vehicle_pass(self):
        company = self._create_company("Company")
        vehicle_pass = self._create_vehicle_pass(company)
        self.assertIsNotNone(vehicle_pass.id)

    def _create_vehicle_pass(self, company):
        vehicle_pass_repository = VehiclePassRepository(DBSession)
        params = dict(company_id=company.id,
                      owner=u"Owner Name",
                      vehicle_year=u'2000',
                      vehicle_model=u'Ford',
                      color=u"Blue",
                      license_plate=u'123341',
                      date_issued=u'22/12/2011',
                      sticker_number=u'212',
                      receipt_number=u'22',
                      month_expired=u'Dec')
        vehicle_pass = create_vehicle_pass(vehicle_pass_repository,
                                           **params)
        return vehicle_pass

    def _create_company(self, name):
        CompanyTuple = namedtuple('CompanyTuple', 'name manager\
                                                     registration_number\
                                                     company_type')
        person = Person("Firstname", "Lastname")
        params = CompanyTuple(name=name, manager=person,
                              registration_number='12311',
                              company_type='Import/Export')
        company = Company(params)
        company.active = True
        repository = CompanyRepository(DBSession)
        repository.persist(company)
        return company
