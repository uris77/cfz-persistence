from nose.tools import eq_
from cfzpersistence.tests import (DaoTest,
                                  create_database,
                                  DBSession)
from cfzpersistence.repository.outlets import OutletsRepository
from cfzpersistence.repository.company import CompanyRepository
from cfzcore.person import Person
from cfzcore.company import Company
from cfzcore.address import Address
from cfzcore.outlet import Outlet
from collections import namedtuple
from cfzcore.builders.outlets import OutletBuilder


class OutletsRepositoryTests(DaoTest):

    def test_should_persist_outlet(self):
        company = self._create_company("Company")
        outlet = self._create_outlet("Outlet", company)
        self.assertIsNotNone(outlet.id)

    def test_should_have_active_set_by_default(self):
        company = self._create_company("Company")
        outlet = self._create_outlet("Outlet", company)
        eq_(True, outlet.active)

    def test_should_list_outlets_in_company(self):
        company = self._create_company('Company')
        for cnt in range(0, 100):
            self._create_outlet("Outlet", company)
        company2 = self._create_company('Company2')
        for cnt in range(0, 10):
            self._create_outlet("Outlet", company2)
        repository = OutletsRepository(DBSession)
        outlets_in_company = repository.all_for_company(company)
        eq_(100, len(outlets_in_company))

    def _create_outlet(self, outlet_name, company):
        from cfzcore.outlet import OutletStatus
        repository = OutletsRepository(DBSession)
        address = Address()
        address.update("Plaza", "Locale")
        outlet = Outlet(outlet_name)
        builder = OutletBuilder(outlet)
        outlet = builder.add_address(address)
        outlet.status = OutletStatus.ACTIVE
        outlet.company = company
        outlet = repository.persist(outlet)
        return outlet

    def _create_company(self, name):
        CompanyTuple = namedtuple('CompanyTuple', 'name manager\
                                                     registration_number\
                                                     company_type')
        person = Person("Firstname", "Lastname")
        params = CompanyTuple(name=name, manager=person,
                              registration_number='12311',
                              company_type='Import/Export')
        company = Company(params)
        company.active = True
        repository = CompanyRepository(DBSession)
        repository.persist(company)
        return company

