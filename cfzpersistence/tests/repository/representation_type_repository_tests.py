from nose.tools import eq_
from cfzpersistence.tests import (DaoTest,
                                  create_database,
                                  DBSession)
from cfzcore.company import RepresentationType
from cfzpersistence.repository.authorized_representative import RepresentationTypeRepository


class RepresentationTypeRepositoryTests(DaoTest):
    def test_it_should_have_an_id_when_persisted(self):
        representation_type = RepresentationType(u'Broker')
        repository = RepresentationTypeRepository(DBSession)
        persisted_representation_type = repository.persist(representation_type)
        self.assertIsNotNone(persisted_representation_type.id)

    def test_it_should_list_all_persisted_representation_types(self):
        broker = RepresentationType(u'Broker')
        director = RepresentationType(u'Director')
        manager = RepresentationType(u'Manager')
        repository = RepresentationTypeRepository(DBSession)
        broker = repository.persist(broker)
        director = repository.persist(director)
        manager = repository.persist(manager)
        representation_types = repository.all()
        eq_(3, len(representation_types))
        eq_(True, broker in representation_types)
        eq_(True, director in representation_types)
        eq_(True, manager in representation_types)

    def test_it_should_retrieve_represenation_type_by_id(self):
        broker = RepresentationType(u'Broker')
        repository = RepresentationTypeRepository(DBSession)
        persisted_broker = repository.persist(broker)
        retrieved_broker = repository.get(persisted_broker.id)
        print "persisted_broker: ", persisted_broker
        print "retrieved_broker: ", retrieved_broker
        eq_(retrieved_broker.id, persisted_broker.id)
