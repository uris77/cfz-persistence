from nose.tools import eq_
from cfzpersistence.tests import (DaoTest,
                                  create_database,
                                  DBSession)
from cfzpersistence.repository.categories import GoodsCategoryRepository
from cfzpersistence.repository.company import CompanyRepository
from cfzcore.goods import GoodsCategory


class GoodsCategoriesTests(DaoTest):

    def test_should_persist_a_category(self):
        liqour_category = self._create_good_category('Liqours')
        self.assertIsNotNone(liqour_category.id)

    def test_should_retrieve_all_categories(self):
        self._create_good_category('Liqours')
        self._create_good_category('Smokies')
        self._create_good_category('Wearables')
        categories = self._all_categories()
        eq_(3, len(categories))

    def test_should_retrieve_category_by_id(self):
        liqour = self._create_good_category('Liqours')
        retrieved_liqour = GoodsCategoryRepository(DBSession).get(liqour.id)
        eq_(liqour.id, retrieved_liqour.id)

    def test_it_should_assign_category_to_company(self):
        company_repository = CompanyRepository(self.DBSession)
        clothing = GoodsCategory('Wearables')
        company = self._create_company()
        company.goods_categories.append(clothing)
        company_repository.persist(company)
        _company = company_repository.get(company.id)
        eq_(1, len(_company.goods_categories),
            "Can tag a company with a category")

    def test_it_should_remove_a_category_from_a_company(self):
        company_repository = CompanyRepository(self.DBSession)
        company = self._create_company()
        clothing = GoodsCategory('Wearables')
        liqour = GoodsCategory('Liqours')
        company.goods_categories.append(clothing)
        company.goods_categories.append(liqour)
        company_repository.persist(company)
        eq_(2, len(company.goods_categories), "Added 2 categories")
        company.goods_categories.remove(liqour)
        company_repository.persist(company)
        _company = company_repository.get(company.id)
        eq_(1, len(_company.goods_categories), "Only 1 category left")
        eq_(True, clothing in _company.goods_categories,
            "The Category left if Wearable")

    def test_it_should_remove_a_category(self):
        repository = GoodsCategoryRepository(DBSession)
        liquors = self._create_good_category('Liqours')
        repository.remove(liquors)
        eq_(False, liquors.active)

    def _all_categories(self):
        repository = GoodsCategoryRepository(DBSession)
        return repository.all()

    def _create_good_category(self, name):
        repository = GoodsCategoryRepository(DBSession)
        liqour_category = GoodsCategory(name)
        return repository.persist(liqour_category)

    def _create_company(self):
        from collections import namedtuple
        from cfzcore.person import Person
        from cfzcore.company import Company
        repository = CompanyRepository(self.DBSession)
        director = Person("Director", "First")
        CompanyTuple = namedtuple(
            "CompanyTuple", "name director registration_number company_type")
        company1_tuple = CompanyTuple(name="Company", director=director,
                                      registration_number="1",
                                      company_type="Import")
        company1 = Company(company1_tuple)
        company1.active = True
        return repository.persist(company1)
