from nose.tools import eq_, raises

from cfzpersistence.tests import DaoTest, clean_database, create_database
from cfzpersistence.repository import NullRecordException
from cfzpersistence.repository.company import CompanyRepository

from cfzcore.person import Person
from cfzcore.company import Company

from collections import namedtuple


class CompanyRepositoryTests(DaoTest):

    def startup(self):
        repository = CompanyRepository(self.DBSession)
        CompanyTuple = namedtuple("CompanyTuple", "name director registration_number company_type")
        company1 = self.create_company(u'Company1')
        company2 = self.create_company(u'Company2')
        company3 = self.create_company(u'Company3')
        company3.active = False
        repository.persist(company3)
        company4 = self.create_company(u'Company4')
        company5 = self.create_company(u'Company5')
        company6 = self.create_company(u'Company6')
        company7 = self.create_company(u'Company7')
        company8 = self.create_company(u'Company8')
        company9 = self.create_company(u'Company9')
        company10 = self.create_company(u'Company10')
        company11 = self.create_company(u'Company11')
        company12 = self.create_company(u'Company12')
        company13 = self.create_company(u'Company13')

    def test_should_get_a_list_of_all_active_companies(self):
        self.startup()
        repository = CompanyRepository(self.DBSession)
        active_companies = repository.all_authorized_companies()
        eq_(12, len(active_companies))

    def test_total_companies_count(self):
        self.startup()
        repository = CompanyRepository(self.DBSession)
        total = repository.total()
        eq_(12, total)

    def test_paginate_with_size_5_and_offset_1(self):
        self.startup()
        repository = CompanyRepository(self.DBSession)
        companies = repository.get_by_pagination(1, 5)
        eq_(5, len(companies))

    def test_paginate_with_size_10_and_offset_10(self):
        self.startup()
        repository = CompanyRepository(self.DBSession)
        companies = repository.get_by_pagination(10, 10)
        eq_(2, len(companies))

    def test_search_by_name_with_size_5_and_offset_0(self):
        self.startup()
        repository = CompanyRepository(self.DBSession)
        companies = repository.search_for(u'Company', 0, 5)
        eq_(5, len(companies))

    def test_search_for_Company12_with_size_10_and_offset_0(self):
        self.startup()
        repository = CompanyRepository(self.DBSession)
        companies = repository.search_for(u'Company12', 0, 12)
        eq_(1, len(companies))

    def test_retrieve_company_given_an_id(self):
        self.startup()
        repository = CompanyRepository(self.DBSession)
        _company = repository.get_by_pagination(0, 1)[0]
        company = repository.get(_company.id)
        eq_(_company.id, company.id)

    def test_should_retrieve_company_annual_reports(self):
        from cfzcore.interactors.company import create_annual_report_for_company
        self.startup()
        repository = CompanyRepository(self.DBSession)
        company = repository.all_authorized_companies()[0]
        report1 = dict(date_submitted=u"21/01/2012",
                       reporting_year=2010)
        report2 = dict(date_submitted=u"01/10/2012",
                       reporting_year=2011)
        create_annual_report_for_company(company, **report1)
        create_annual_report_for_company(company, **report2)
        eq_(2, len(repository.annual_reports_for_company(company.id)))

    @raises(NullRecordException)
    def test_should_raise_NullRecordException_if_record_does_not_exist(self):
        repository = CompanyRepository(self.DBSession)
        repository.get(10000)

