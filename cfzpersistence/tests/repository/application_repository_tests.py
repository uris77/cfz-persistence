from nose.tools import eq_, raises

from cfzpersistence.repository import InvalidIdTypeException
from cfzpersistence.tests import DaoTest, clean_database, create_database
from cfzpersistence.repository.application import ApplicationRepository

from cfzcore.interactors.application_builder import ApplicationFormBuilder
from cfzcore.interactors.application import ApplicationBasicInformation
from cfzcore.application import Status
from cfzcore.person import Person


class ApplicationRepositoryTests(DaoTest):

    def test_should_persist_application_when_it_is_created(self):
        application = self.__create_application__()
        self.assertIsNotNone(application.id)
        self.assertIsNotNone(application.company.id)
        eq_('New', application.status)
        eq_('Company', application.company.name)

    def test_application_should_have_created_date(self):
        application = self.__create_application__()
        self.assertIsNotNone(application.created_date)

    def test_company_should_have_created_date_when_application_is_created(self):
        application = self.__create_application__()
        self.assertIsNotNone(application.company.created_date)

    def test_company_should_not_be_active_when_application_is_created(self):
        application = self.__create_application__()
        print "\napplication: ", application
        eq_(False, application.company.active)

    def test_company_deleted_flag_should_be_false_when_application_is_created(self):
        application = self.__create_application__()
        eq_(False, application.company.deleted)

    def test_should_list_all_applications_with_status_new(self):
        self.__create_application__()
        self.__create_application__()
        applications = self.application_repository.all_with_status_new()
        eq_(2, len(applications))

    def test_list_ignore_applications_with_status_not_equal_to_new(self):
        self.__create_application__()
        application_tuple = self.__create_application__()
        repository = ApplicationRepository(self.DBSession)
        params = {'status': Status.PENDING,
                  'application_id': application_tuple.id}
        basic_information_form = ApplicationBasicInformation(repository)
        location_params = {'country': 'Belize', 'state': 'Belize',
                           'application_id': application_tuple.id}
        number_params = dict(telephone_number="123123",
                             application_id=application_tuple.id)
        basic_information_form.save_contact_numbers(**number_params)
        basic_information_form.save_company_location(**location_params)
        basic_information_form.update_basic_information(**params)
        applications = self.application_repository.all_with_status_new()
        eq_(1, len(applications))

    def test_list_all_pending_applications(self):
        self.__create_application__()
        basic_information_form = ApplicationBasicInformation(self.application_repository)
        application = self.__create_application__()
        params = {'status': Status.PENDING,
                  'application_id': application.id}
        number_params = dict(telephone_number="123123",
                             application_id=application.id)
        location_params = {'country': 'Belize',
                           'state': 'Belize',
                           'application_id': application.id}
        basic_information_form.save_contact_numbers(**number_params)
        basic_information_form.save_company_location(**location_params)
        basic_information_form.update_basic_information(**params)
        applications = self.application_repository.all_with_status_pending()
        eq_(1, len(applications))

    def test_retrieve_application_record_with_id(self):
        application = self.__create_application__()
        repository = ApplicationRepository(self.DBSession)
        application = repository.get(application.id)
        self.assertIsNotNone(application)

    def test_remove_investor(self):
        application_tuple = self.__create_application__()
        repository = ApplicationRepository(self.DBSession)
        investor = Person("First", "Investor")
        application = repository.get(application_tuple.id)
        application.assign_investor(investor, 50)
        repository.persist(application)
        application_investor = application.investors[0]
        self.assertIsNotNone(application_investor.id)
        repository.delete_investor(application_investor)
        eq_(0, len(application.investors))

    @raises(InvalidIdTypeException)
    def test_should_fail_when_id_does_not_exist(self):
        repository = ApplicationRepository(self.DBSession)
        repository.get('d')

    def test_should_list_all_denied_applications(self):
        self.__create_application__()
        application_tuple = self.__create_application__()
        application = self.application_repository.get(application_tuple.id)
        application.status = Status.DENIED
        _applications = self.application_repository.all_denied_applications()
        print "_applications: ", _applications
        self.application_repository.persist(application)
        applications = self.application_repository.all_denied_applications()
        eq_(1, len(applications))

    def __create_application__(self):
        self.application_repository = ApplicationRepository(self.DBSession)
        self.application_builder = ApplicationFormBuilder(self.application_repository)
        params = {'name': 'Company',
                  'company_type': 'Import/Export',
                  'registration_number': '1',
                  'first_name': 'Manager',
                  'last_name': 'Last',
                  'submission_date': '30/01/2012'}
        application = self.application_builder.create_application(**params)
        return application
