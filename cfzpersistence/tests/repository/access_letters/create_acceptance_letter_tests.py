from cfzpersistence.tests import (
    DaoTest,
    DBSession,
    create_database)
from cfzcore.letters import (
    AccessLetter,
    Executives,
    ProcessingFee)
from cfzpersistence.repository.letters import AccessLetterRepository


class CreateAcceptanceLetterTests(DaoTest):
    def test_should_persist_an_acceptance_letter(self):
        letter = AccessLetter()
        letter.granted_to = u"Visitor Jones"
        letter.vehicle_license_plate = u"D1231"
        letter.processing_fee = ProcessingFee.paid
        letter.receipt_id = u"1123"
        letter.authorized_by = Executives.chairman
        letter.issued_by = u"Username"
        repository = AccessLetterRepository(DBSession)
        letter = repository.persist(letter)
        self.assertIsNotNone(letter.id)
