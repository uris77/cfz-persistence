from nose.tools import eq_
from cfzpersistence.tests import (
    DaoTest,
    DBSession,
    create_database)
from cfzcore.letters import (
    AccessLetter,
    Executives,
    ProcessingFee,
    AccessLetterType)
from cfzpersistence.repository.letters import AccessLetterRepository
from datetime import datetime, timedelta


class RetrieveAccessLettersTests(DaoTest):
    def test_should_retrieve_letter_by_id(self):
        repository = AccessLetterRepository(DBSession)
        letter = self._create_letter(**dict())
        retrieved_letter = repository.get(letter.id)
        eq_(retrieved_letter.id, letter.id)

    def test_should_list_letters_by_type(self):
        self._create_courtesy_letters(42)
        self._create_temporary_work_letters(30)
        repository = AccessLetterRepository(DBSession)
        courtesy_letters = repository.list_by_letter_type(
            AccessLetterType.courtesy)
        temporary_work_letters = repository.list_by_letter_type(
            AccessLetterType.temporary_workers)
        eq_(42, len(courtesy_letters))
        eq_(30, len(temporary_work_letters))

    def test_should_not_list_expired_letters(self):
        repository = AccessLetterRepository(DBSession)
        self._create_courtesy_letters(37)
        params = dict(
            expiration_date=datetime.strptime('25/5/2010', '%d/%m/%Y'))
        expired_letter = self._create_letter(**params)
        retrieved_letters = repository.list_by_letter_type(
            AccessLetterType.courtesy)
        eq_(37, len(retrieved_letters))
        eq_(False, expired_letter in retrieved_letters)

    def test_should_retrieve_all_expired_letters(self):
        repository = AccessLetterRepository(DBSession)
        self._create_courtesy_letters(37)
        params = dict(
            expiration_date=datetime.strptime('25/5/2010', '%d/%m/%Y'))
        for cnt in range(0, 13):
            self._create_letter(**params)
        retrieved_letters = repository.list_expired_letters(
            AccessLetterType.courtesy)
        eq_(13, len(retrieved_letters))

    def _create_courtesy_letters(self, qty):
        for cnt in range(0, qty):
            self._create_letter(
                **dict(letter_type=AccessLetterType.courtesy))

    def _create_temporary_work_letters(self, qty):
        for cnt in range(0, qty):
            self._create_letter(
                **dict(letter_type=AccessLetterType.temporary_workers))

    def _create_letter(self, **kwargs):
        ten_days = timedelta(days=10)
        letter = AccessLetter()
        letter.granted_to = kwargs.get('granted_to', self._random_string())
        letter.vehicle_license_plate = kwargs.get(
            'license_plate',
            self._random_string())
        letter.processing_fee = kwargs.get(
            'processing_fee',
            ProcessingFee.paid)
        letter.receipt_id = kwargs.get('receipt_id', self._random_string())
        letter.authorized_by = kwargs.get(
            'authorized_by',
            Executives.chairman)
        letter.issued_by = kwargs.get('issued_by', self._random_string())
        letter.letter_type = kwargs.get(
            'letter_type',
            AccessLetterType.courtesy)
        letter.expiration_date = kwargs.get(
            'expiration_date', datetime.now() + ten_days)
        repository = AccessLetterRepository(DBSession)
        return repository.persist(letter)

    def _random_string(self):
        import random
        import string
        return u''.join(random.choice(string.lowercase) for i in range(20))
