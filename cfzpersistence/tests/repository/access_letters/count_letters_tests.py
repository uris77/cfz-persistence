from nose.tools import eq_
from cfzpersistence.tests import (
    DaoTest,
    DBSession,
    create_database)
from cfzcore.letters import (
    AccessLetter,
    Executives,
    ProcessingFee,
    AccessLetterType)
from cfzpersistence.repository.letters import AccessLetterRepository
from datetime import datetime, timedelta


class CountLettersTests(DaoTest):

    def test_returns_total_count_of_letters(self):
        self._create_courtesy_letters(54)
        repository = AccessLetterRepository(DBSession)
        total = repository.count_by_letter_type(AccessLetterType.courtesy)
        letters = repository.list_by_letter_type(
            AccessLetterType.courtesy, 0, 10)
        eq_(54, total)
        twenty_days = timedelta(days=20)
        for letter in letters:
            letter.expiration_date = datetime.now() - twenty_days
            repository.persist(letter)
        total = repository.count_by_letter_type(AccessLetterType.courtesy)
        eq_(44, total)

    def _create_courtesy_letters(self, qty):
        for cnt in range(0, qty):
            self._create_letter(
                **dict(letter_type=AccessLetterType.courtesy))

    def _create_letter(self, **kwargs):
        ten_days = timedelta(days=10)
        letter = AccessLetter()
        letter.granted_to = kwargs.get('granted_to', self._random_string())
        letter.vehicle_license_plate = kwargs.get(
            'license_plate',
            self._random_string())
        letter.processing_fee = kwargs.get(
            'processing_fee',
            ProcessingFee.paid)
        letter.receipt_id = kwargs.get('receipt_id', self._random_string())
        letter.authorized_by = kwargs.get(
            'authorized_by',
            Executives.chairman)
        letter.issued_by = kwargs.get('issued_by', self._random_string())
        letter.letter_type = kwargs.get(
            'letter_type',
            AccessLetterType.courtesy)
        letter.expiration_date = kwargs.get(
            'expiration_date', datetime.now() + ten_days)
        repository = AccessLetterRepository(DBSession)
        return repository.persist(letter)

    def _random_string(self):
        import random
        import string
        return u''.join(random.choice(string.lowercase) for i in range(20))
