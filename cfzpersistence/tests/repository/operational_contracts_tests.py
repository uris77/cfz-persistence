from nose.tools import eq_, raises
from cfzpersistence.tests import (DaoTest,
                                  create_database,
                                  DBSession)

from cfzpersistence.repository.operational_contract import OperationalContractRepository
from cfzcore.company.docs.contracts import OperationalContract
from cfzpersistence.repository import NullRecordException


class OperationalContractsRepositoryTests(DaoTest):
    def setUp(self):
        super(OperationalContractsRepositoryTests, self).setUp()
        self.repo = OperationalContractRepository(DBSession)

    def test_should_persist_a_contract(self):
        company = self.create_company(u'Company')
        params = {
                'company': company,
                'cfz_ceo': u'Cfz CEO',
                'director': u'Director Name',
                'cfz_office': u'CFZ Office',
                'company_types': u'Import/Export',
                'goods': u'Footwear',
                'seal_date': '01/12/2012',
                'processing_fee': '6.00'
                }
        operational_contract = OperationalContract.create_with(**params)
        operational_contract = self.repo.persist(operational_contract)
        self.assertIsNotNone(operational_contract.id)

    def test_should_add_contract_to_company(self):
        company = self.create_company(u'Company')
        params = {
                'company': company,
                'cfz_ceo': u'Cfz CEO',
                'director': u'Director Name',
                'cfz_office': u'CFZ Office',
                'company_types': u'Import/Export',
                'goods': u'Footwear',
                'seal_date': '01/12/2012',
                'processing_fee': '6.00'
                }
        operational_contract = OperationalContract.create_with(**params)
        company.operational_contracts.append(operational_contract)
        eq_(1, len(company.operational_contracts))


    def test_should_retrieve_contract_by_id(self):
        company = self.create_company(u'Company')
        params = {
                'company': company,
                'cfz_ceo': u'Cfz CEO',
                'director': u'Director Name',
                'cfz_office': u'CFZ Office',
                'company_types': u'Import/Export',
                'goods': u'Footwear',
                'seal_date': '01/12/2012',
                'processing_fee': '6.00'
                }
        operational_contract = OperationalContract.create_with(**params)
        operational_contract = self.repo.persist(operational_contract)
        retrieved = self.repo.get(operational_contract.id)
        eq_(operational_contract.id, retrieved.id)

    @raises(NullRecordException)
    def test_should_raise_exception_if_record_does_not_exist(self):
        self.repo.get(92123123)

