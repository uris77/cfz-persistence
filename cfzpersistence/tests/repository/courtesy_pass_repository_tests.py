from nose.tools import eq_
from cfzpersistence.tests import (DaoTest,
                                  create_database,
                                  DBSession)

from cfzcore.company.passes import CourtesyPass
from cfzpersistence.repository.passes import CourtesyPassRepository

class CourtesyPassRepositoryTests(DaoTest):
    def test_it_should_persist_courtesy_pass(self):
        courtesy_pass = self._create_courtesy_pass(self.create_company(u'Company'))
        self.assertIsNotNone(courtesy_pass.id)

    def test_it_should_retrieve_courtesy_pass(self):
        repository = CourtesyPassRepository(DBSession)
        courtesy_pass = self._create_courtesy_pass(self.create_company(u'Company'))
        retrieved_courtesy_pass = repository.get(courtesy_pass.id)
        eq_(retrieved_courtesy_pass.id, courtesy_pass.id)

    def test_it_should_list_courtesy_passes_for_company(self):
        repository = CourtesyPassRepository(DBSession)
        company = self.create_company(u'Company')
        courtesy_pass1 = self._create_courtesy_pass(company)
        courtesy_pass2 = self._create_courtesy_pass(company)
        courtesy_pass3 = self._create_courtesy_pass(company)
        passes = repository.all_for_company(company)
        eq_(3, len(passes))
        eq_(True, courtesy_pass1 in passes)

    def _create_courtesy_pass(self, company):
        repository = CourtesyPassRepository(DBSession)
        params = dict(company = company,
                      person = u'Person',
                      vehicle_year = u'2010',
                      vehicle_model = u'Toyota',
                      color = u'Blue',
                      license_plate = u'1212',
                      date_issued = u'21/09/2012',
                      sticker_number = u'1221',
                      month_expired = u'December',
                      issued_by = u'Jane Smith',
                      authorized_by = u'Mr. CEO')
        courtesy_pass = CourtesyPass.create(**params)
        return repository.persist(courtesy_pass)
