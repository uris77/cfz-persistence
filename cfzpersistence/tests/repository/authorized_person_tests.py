from nose.tools import eq_
from cfzpersistence.tests import (DaoTest,
                                  create_database,
                                  DBSession)

from cfzcore.company import AuthorizedRepresentative
from cfzcore.company import RepresentationType
from cfzpersistence.repository.authorized_representative import AuthorizedRepresentativeRepository

class CreateAuthorizedPersonTests(DaoTest):
    def test_it_should_persist_an_authorized_peson(self):
        repository = AuthorizedRepresentativeRepository(DBSession)
        representative = self._create_representation_type(u'Legal Rep')
        company = self.create_company(u"Company")
        args = dict(company=company,
                    first_name=u"John",
                    last_name=u"Doe",
                    representation_type=representative)
        authorized_representative = AuthorizedRepresentative.create(**args)
        authorized_representative = repository.persist(authorized_representative)
        self.assertIsNotNone(authorized_representative.id)

    def test_it_should_list_company_representatives(self):
        repository = AuthorizedRepresentativeRepository(DBSession)
        representative = self._create_representation_type(u'Legal Rep')
        company = self.create_company(u"Company")
        args = dict(company=company,
                    first_name=u"John",
                    last_name=u"Doe",
                    representation_type=representative)
        legal = AuthorizedRepresentative.create(**args)
        legal = repository.persist(legal)
        args = dict(company=company,
                    first_name=u"John",
                    last_name=u"Doe",
                    representation_type=representative)
        broker = AuthorizedRepresentative.create(**args)
        broker = repository.persist(broker)
        eq_(2, len(repository.all_for_company(company)))

    def test_it_should_retrieve_a_representative(self):
        repository = AuthorizedRepresentativeRepository(DBSession)
        representative = self._create_representation_type(u'Legal Rep')
        company = self.create_company(u"Company")
        args = dict(company=company,
                    first_name=u"John",
                    last_name=u"Doe",
                    representation_type=representative)
        authorized_representative = AuthorizedRepresentative.create(**args)
        authorized_representative = repository.persist(authorized_representative)
        retrieved_representative = repository.get(authorized_representative.id)
        eq_(authorized_representative.id, retrieved_representative.id)

    def test_it_should_set_active_field_to_false_when_deleting(self):
        deleted_representative = self._delete_authorized_representative()
        eq_(False, deleted_representative.active)

    def test_it_should_have_date_deleted_value_when_deleted(self):
        deleted_representative = self._delete_authorized_representative()
        self.assertIsNotNone(deleted_representative.date_deleted)

    def _delete_authorized_representative(self):
        repository = AuthorizedRepresentativeRepository(DBSession)
        representative = self._create_representation_type(u'Legal Rep')
        company = self.create_company(u'Company')
        args = dict(company=company,
                    representation_type=representative,
                    first_name=u'John',
                    last_name=u'Doe')
        authorized_representative = AuthorizedRepresentative.create(**args)
        authorized_representative = repository.persist(authorized_representative)
        return repository.delete(authorized_representative)


    def _create_representation_type(self, name):
        from cfzpersistence.repository.authorized_representative import RepresentationTypeRepository
        repository = RepresentationTypeRepository(DBSession)
        return repository.persist(RepresentationType(name))

