def create_user_groups(DBSession):
    from cfzpersistence.repository.auth import UserGroupRepository
    from cfzcore.user_account import UserGroup
    usergroup_repo = UserGroupRepository(DBSession)
    if not usergroup_repo.find(u'admin'):
        admin_group = UserGroup(u'admin')
        usergroup_repo.persist(admin_group)
    if not usergroup_repo.find(u'company:create'):
        company_create_group = UserGroup(u'company:create')
        usergroup_repo.persist(company_create_group)
    if not usergroup_repo.find(u'company:view'):
        company_view_group = UserGroup(u'company:view')
        usergroup_repo.persist(company_view_group)
    if not usergroup_repo.find(u'authrep:create'):
        authrep_create_group = UserGroup(u'authrep:create')
        usergroup_repo.persist(authrep_create_group)
    if not usergroup_repo.find(u'authrep:view'):
        authrep_view_group = UserGroup(u'authrep:view')
        usergroup_repo.persist(authrep_view_group)
    if not usergroup_repo.find(u'company:contact:view'):
        company_contact_view_group = UserGroup(u'company:contact:view')
        usergroup_repo.persist(company_contact_view_group)
    if not usergroup_repo.find(u'company:contact:edit'):
        company_contact_edit_group = UserGroup(u'company:contact:edit')
        usergroup_repo.persist(company_contact_edit_group)
    if not usergroup_repo.find(u'company:address:edit'):
        company_address_edit_group = UserGroup(u'company:address:edit')
        usergroup_repo.persist(company_address_edit_group)
    if not usergroup_repo.find(u'company:address:view'):
        company_address_view_group = UserGroup(u'company:address:view')
        usergroup_repo.persist(company_address_view_group)
    if not usergroup_repo.find(u'company:director:view'):
        company_director_view_group = UserGroup(u'company:director:view')
        usergroup_repo.persist(company_director_view_group)
    if not usergroup_repo.find(u'company:director:create'):
        company_director_create_group = UserGroup(u'company:director:create')
        usergroup_repo.persist(company_director_create_group)
    if not usergroup_repo.find(u'company:director:edit'):
        company_director_edit_group = UserGroup(u'company:director:edit')
        usergroup_repo.persist(company_director_edit_group)
    if not usergroup_repo.find(u'company:shareholder:view'):
        company_shareholder_view_group = UserGroup(u'company:shareholder:view')
        usergroup_repo.persist(company_shareholder_view_group)
    if not usergroup_repo.find(u'company:shareholder:create'):
        company_shareholder_create_group = UserGroup(u'company:shareholder:create')
        usergroup_repo.persist(company_shareholder_create_group)
    if not usergroup_repo.find(u'company:shareholder:edit'):
        company_shareholder_edit_group = UserGroup(u'company:shareholder:edit')
        usergroup_repo.persist(company_shareholder_edit_group)
    if not usergroup_repo.find(u'company:goods:edit'):
        company_goods_edit_group = UserGroup(u'company:goods:edit')
        usergroup_repo.persist(company_goods_edit_group)
    if not usergroup_repo.find(u'company:goods:view'):
        company_goods_view_group = UserGroup(u'company:goods:view')
        usergroup_repo.persist(company_goods_view_group)
    if not usergroup_repo.find(u'company:goods:create'):
        company_goods_create_group = UserGroup(u'company:goods:create')
        usergroup_repo.persist(company_goods_create_group)
    if not usergroup_repo.find(u'company:annualreport:create'):
        company_annualreport_create_group = UserGroup(u'company:annualreport:create')
        usergroup_repo.persist(company_annualreport_create_group)
    if not usergroup_repo.find(u'company:annualreport:view'):
        company_annualreport_view_group = UserGroup(u'company:annualreport:view')
        usergroup_repo.persist(company_annualreport_view_group)
    if not usergroup_repo.find(u'company:courtesysticker:view'):
        company_courtesysticker_view_group = UserGroup(u'company:courtesysticker:view')
        usergroup_repo.persist(company_courtesysticker_view_group)
    if not usergroup_repo.find(u'company:courtesysticker:edit'):
        company_courtesysticker_edit_group = UserGroup(u'company:courtesysticker:edit')
        usergroup_repo.persist(company_courtesysticker_edit_group)
    if not usergroup_repo.find(u'company:courtesysticker:create'):
        company_courtesysticker_create_group = UserGroup(u'company:courtesysticker:create')
        usergroup_repo.persist(company_courtesysticker_create_group)
    if not usergroup_repo.find(u'company:vehiclesticker:view'):
        company_vehiclesticker_view_group = UserGroup(u'company:vehiclesticker:view')
        usergroup_repo.persist(company_vehiclesticker_view_group)
    if not usergroup_repo.find(u'company:vehiclesticker:edit'):
        company_vehiclesticker_edit_group = UserGroup(u'company:vehiclesticker:edit')
        usergroup_repo.persist(company_vehiclesticker_edit_group)
    if not usergroup_repo.find(u'company:vehiclesticker:create'):
        company_vehiclesticker_create_group = UserGroup(u'company:vehiclesticker:create')
        usergroup_repo.persist(company_vehiclesticker_create_group)
    if not usergroup_repo.find(u'company:outlet:create'):
        company_outlet_create_group = UserGroup(u'company:outlet:create')
        usergroup_repo.persist(company_outlet_create_group)


def create_company_types(DBSession):
    from cfzpersistence.repository.company_type import CompanyTypeRepository
    from cfzcore.company import CompanyType
    repo = CompanyTypeRepository(DBSession)
    if not repo.find_by_name(u'Import/Export'):
        repo.persist(CompanyType.create(u'Import/Export'))
    if not repo.find_by_name(u'Manufacturing'):
        repo.persist(CompanyType.create(u'Manufacturing'))
    if not repo.find_by_name(u'Rental of Space'):
        repo.persist(CompanyType.create(u'Rental of Space'))
    if not repo.find_by_name(u'Telecommunications'):
        repo.persist(CompanyType.create(u'Telecommunications'))
    if not repo.find_by_name(u'Radio'):
        repo.persist(CompanyType.create(u'Radio'))
    if not repo.find_by_name(u'Banking'):
        repo.persist(CompanyType.create(u'Banking'))
    if not repo.find_by_name(u'Gaming'):
        repo.persist(CompanyType.create(u'Gaming'))
    if not repo.find_by_name(u'ISP'):
        repo.persist(CompanyType.create(u'ISP'))
    if not repo.find_by_name(u'Freight Services'):
        repo.persist(CompanyType.create(u'Freight Services'))
