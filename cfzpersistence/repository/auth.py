from cfzpersistence.repository import Repository
from cfzcore.user_account import UserAccount, UserGroup
from cfzcore.persistence.repository import UserAccountAbstractRepository
from sqlalchemy.sql.expression import and_


class UserAccountRepository(Repository, UserAccountAbstractRepository):
    
    def find(self, user_email):
        return self.DBSession.query(UserAccount).\
                filter(and_(UserAccount.email == user_email,\
                UserAccount.active == True)).first()

    def all(self):
        return self.DBSession.query(UserAccount).\
                filter(UserAccount.active == True).all()

    def get(self, id):
        return self.DBSession.query(UserAccount).\
                filter(UserAccount.id == int(id)).first()

    def is_email_available(self, email):
        user = self.find(email)
        if user:
            return False
        else:
            return True

    def persist(self, user):
        if self.is_email_available(user.email) is False and getattr(user, 'id', None) is None: 
            raise DuplicateUserEmailException("User with email already exists!")
        else:
            return super(UserAccountRepository, self).persist(user)

    def remove_permissions(self, user, permissions):
        for permission in permissions:
            if permission in user.groups: user.groups.remove(permission)

    def add_permissions(self, user, permissions):
        for permission in permissions:
            if permission not in user.groups: user.add_to_group(permission)
        return self.persist(user)


class UserGroupRepository(Repository):
    
    def find(self, group_name):
        return self.DBSession.query(UserGroup).\
                filter(UserGroup.group_name == group_name).first()

    def get(self, id):
        return self.DBSession.query(UserGroup).\
                filter(UserGroup.id == int(id)).first()


class DuplicateUserEmailException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)
