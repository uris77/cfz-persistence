from cfzpersistence.repository import Repository

from cfzcore.application import ApplicationInvestor


class InvestorRepository(Repository):
    def get(self, id):
        '''
        Retrieve an instance of investor from the database.
        '''
        investor = self.DBSession.query(ApplicationInvestor).get(int(id))
        return investor
