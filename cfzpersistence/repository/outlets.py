from cfzpersistence.repository import Repository
from cfzcore.persistence.repository import (OutletsAbstractRepository,
                                            OutletVehiclepassAbstractRepository)
from sqlalchemy.sql.expression import and_
from cfzcore.outlet import (Outlet, OutletVehiclepass)


class OutletsRepository(Repository, OutletsAbstractRepository):
    def find_by_id(self, id):
        return self.DBSession.query(Outlet).\
                filter(Outlet.id == int(id)).first()

    def all_for_company(self, company):
        return self.DBSession.query(Outlet).\
                filter(and_(Outlet.active == True,
                            Outlet.company == company)).all()


class OutletVehiclepassRepository(Repository, OutletVehiclepassAbstractRepository):
    
    def all(self):
        pass

    def all_for_outlet(self, outlet):
        return self.DBSession.query(OutletVehiclepass).\
                filter(OutletVehiclepass.outlet == outlet).all()

    def get(self, id):
        return self.DBSession.query(OutletVehiclepass).\
                filter(OutletVehiclepass.id == int(id)).first()

    def search_by_owner(self, owner):
        pass
