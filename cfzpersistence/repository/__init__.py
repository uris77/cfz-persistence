__all__ = ["NullRecordException"]


class NullRecordException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class InvalidIdTypeException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class Repository(object):
    def __init__(self, DBSession):
        self.DBSession = DBSession

    def persist(self, entity):
        '''Persist an entity'''
        self.DBSession.add(entity)
        self.DBSession.flush()
        return entity
