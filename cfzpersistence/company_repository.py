from cfzcore.person import Person
from cfzcore.company import Company
from cfzpersistence.mappings import DBSession
from cfzpersistence.dao import NullRecordException

def insert(**kwargs):
    manager = Person(**kwargs['manager'])
    kwargs['manager'] = manager
    company = Company(**kwargs)
    DBSession.add(company)
    DBSession.flush()
    return company.to_dict()

def update_general_information(**kwargs):
    company = get(kwargs['id'])
    company.update_general_information(**kwargs)
    DBSession.add(company)
    DBSession.flush()
    return company.to_dict()

def get(id):
    company = DBSession.query(Company).get(id)
    if company is None:
        raise NullRecordException("Application Record was not found.")
    return company


